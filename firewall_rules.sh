#! /bin/bash

IPTABLES=/sbin/iptables

WANIF='ens803f0np0'
LANIF='ens802f1'

# enable ip forwarding in the kernel
echo 'Enabling Kernel IP forwarding...'
/bin/echo 1 > /proc/sys/net/ipv4/ip_forward

# flush rules and delete chains
echo 'Flushing rules and deleting existing chains...'
$IPTABLES -F
$IPTABLES -X

# enable masquerading to allow LAN internet access
echo 'Enabling IP Masquerading and other rules...'
$IPTABLES -t nat -A POSTROUTING -o $LANIF -j MASQUERADE
$IPTABLES -A FORWARD -i $LANIF -o $WANIF -m state --state RELATED,ESTABLISHED -j ACCEPT
$IPTABLES -A FORWARD -i $WANIF -o $LANIF -j ACCEPT

$IPTABLES -t nat -A POSTROUTING -o $WANIF -j MASQUERADE
$IPTABLES -A FORWARD -i $WANIF -o $LANIF -m state --state RELATED,ESTABLISHED -j ACCEPT
$IPTABLES -A FORWARD -i $LANIF -o $WANIF -j ACCEPT

# open firewall on port 80 and 443
$IPTABLES -I INPUT -i $WANIF -p tcp --dport 80 -m comment --comment "# HTTP  Web Server Interface #" -j ACCEPT
$IPTABLES -I INPUT -i $WANIF -p tcp --dport 443 -m comment --comment "# HTTPS Web Server Interface #" -j ACCEPT

# restart docker to restore docker rules
systemctl restart docker.service

$IPTABLES-save

echo 'Done.'
