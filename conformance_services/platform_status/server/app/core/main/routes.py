#!/usr/bin/env python3

# Copyright (C) Intel Corp.  2014.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Yuriy Bogdanov <yuriy.bogdanov@intel.com>

import time
import json
import hashlib
import requests
from datetime import datetime
from flask import render_template, request, current_app
from core.main import bp as main
from core.extensions import db
from core.models import database
from sqlalchemy.exc import ProgrammingError


proxies = {
            "http"  : 'http://proxy-jf.intel.com:911', 
            "https" : 'http://proxy-jf.intel.com:912', 
            "ftp"   : 'http://proxy-jf.intel.com:911'
            }

def check_sha(filename):
    """

    Parameters
    ----------
    filename:str

    Returns
    -------
        sha256.hexdigest() is returned string containing only hexadecimal digits.
    """
    return hashlib.sha256(filename).hexdigest()


@main.app_template_filter('time')
def time_filter(s):
    print(f"passsed S={s}")
    s = int(time.mktime(datetime.now().timetuple()) -
            time.mktime(datetime.utcfromtimestamp(s).timetuple()))
    return time.strftime("%H:%M:%S", time.gmtime(s))


def query_all(a_database, **kwargs): # WIP 
    if kwargs:
        for key, value in kwargs.items():
            print(f"63. key: {key}, value: {value}")
    return database.a_database.query.all()


def update_global_config():
    file_check = 'https://gitlab.freedesktop.org/Mesa_CI/mesa_ci_docker/-/raw/master/conformance_services/platform_status/client/app/send_client_info.py'

    try:
        current_app.logger.debug(f"{request.remote_addr} > Connecting to git server with PROXY")
        git_request = requests.get(file_check, timeout=5, proxies=proxies)
    except requests.exceptions.Timeout:
        current_app.logger.debug(f"{request.remote_addr} > Timeout: Connecting to git server")
        git_request = requests.get(file_check, timeout=5)
    except requests.exceptions.ConnectionError:
        current_app.logger.debug(f"{request.remote_addr} > NoProxy: Connecting to git server")
        git_request = requests.get(file_check, timeout=5)
    except requests.exceptions.ProxyError:
        current_app.logger.warning(f"{request.remote_addr} > Failed to Connect to git server")

    global_conf = database.GlobalInfo.query.filter_by(id=1).first()

    # Get File continent SHA
    _file_hex = check_sha(git_request.content)

    if global_conf == [] or global_conf is None:
        current_app.logger.debug("Creating global file version SHA")
        _version = database.GlobalInfo(file_version_sha = _file_hex,
                                       last_updated=datetime.now()
                                       )
        db.session.add(_version)
        db.session.commit()
    else:
        if global_conf.file_version_sha != _file_hex:
            current_app.logger.debug("updating global file version SHA")
            global_conf.file_version_sha = _file_hex
            global_conf.last_updated = datetime.now()
            db.session.add(global_conf)
            db.session.commit()
        else:
            current_app.logger.debug("updating last checked timestamp")
            global_conf.last_updated = datetime.now()
            db.session.add(global_conf)
            db.session.commit()


@main.before_request
def before_request():
    try:
        database.GlobalInfo.query.all()
    except ProgrammingError:
        current_app.logger.info("Creating database")
        db.create_all()


@main.route('/version')
def version():
    global_conf = database.GlobalInfo.query.filter_by(id=1).first()
    try:
        if not global_conf:
            current_app.logger.debug("Initializing... Setting up global Server configuration.")
            update_global_config()

        time_delta = int(datetime.now().timestamp()) - int(global_conf.last_updated.timestamp())
        if time_delta > 600: # default 3600, update every hour. secondary 600, is 10 min
            update_global_config()

        return {
            'time' : global_conf.platform_poll_time_sec,
            'version' : global_conf.file_version_sha
            }
    except AttributeError:
        current_app.logger.warning("Failed to update server details, Retrying!!!")


@main.route('/')
def index():
    db.create_all()
    timestamp = []
    device_index = database.DeviceIndex.query.all()
    devices = database.Devices.query.all()
    disks = database.DiskPartition.query.all()
    networks = database.Networks.query.all()
    peripherals = database.PeripheralDevices.query.all()
    sys_users = database.DeviceUsers.query.all()

    for _, _index in enumerate(device_index):
        time_offset = int(time.mktime(datetime.now().timetuple()) -
                          time.mktime(_index.last_seen.timetuple()))
        get_time_ofset = {
            'id' : _index.id,
            'offset' : time_offset
        }
        timestamp.append(get_time_ofset)

    return render_template('main/index.html',
                           device_index=device_index,
                           devices=devices,
                           disks=disks,
                           networks=networks,
                           peripherals=peripherals,
                           sys_users=sys_users,
                           timestamp=timestamp
                           )


# @main.context_processor
@main.route('/platform', methods = ['GET', 'POST', 'DELETE'])
def platform():
    try:
        database.DeviceIndex.query.all()
    except Exception as err:
        db.create_all()
        database.DeviceIndex.query.all()

    if request.method == 'GET':
        return json.dumps({'success':True}), 403, {'ContentType':'application/json'}

    if request.method == 'POST':
        data = request.get_json(force=True)
        _platform = data['platform']
        _bios = data["bios"]
        _cpu = data["cpu"]
        _installed_memory = data["installed_memory"]
        _swap_memory = data["swap_memory"]
        _gpu = data["gpu"]
        _gpu_str = ""
        _gpu_str =  "\n".join(str(x.strip()) for x in _gpu)
        _disk_info = data["disk_info"]
        _disk_usage = data["disk_usage"]
        _disk_partition = data["disk_partitions"]
        _network = data["network"]
        peripheral_devices = data["peripheral_devices"]
        users = data["users"]

        # generate or retrieve current index for device and update time
        try:
            db_device_index = database.DeviceIndex.query.filter_by(host_name=_platform['node']).first()
            host_id = db_device_index.id

            # update last seen platform timestamp
            db_device_index.last_seen = datetime.now()
            db.session.add(db_device_index)
            db.session.commit()
        except AttributeError:
            # Create index for POST platform if it doesn't exist
            _device_index = database.DeviceIndex(host_name = _platform['node'])
            db.session.add(_device_index)
            db.session.commit()
            host_id = _device_index.id
            db.session.add(database.Devices(id = host_id, host_name = _platform['node']))
            _device_index.last_seen = datetime.now()
            db.session.commit()

        db_device = database.Devices.query.filter_by(host_name=_platform['node']).first()
        if db_device:
            # update existing database
            if db_device.id is not host_id:
                db_device.id = host_id
            if db_device.host_name is not _platform['node']:
                db_device.host_name = _platform['node']
            if db_device.platform_system is not _platform['system']:
                db_device.platform_system = _platform['system']
            if db_device.platform_release is not _platform['release']:
                db_device.platform_release = _platform['release']
            if db_device.platform_version is not _platform['version']:
                db_device.platform_version = _platform['version']
            if db_device.platform_machine is not _platform['machine']:
                db_device.platform_machine = _platform['machine']

            if db_device.bios_vendor is not _bios['vendor']:
                db_device.bios_vendor = _bios['vendor']
            if db_device.bios_release_date is not _bios['release_date']:
                db_device.bios_release_date = _bios['release_date']
            if db_device.bios_version is not _bios['version']:
                db_device.bios_version = _bios['version']

            if db_device.cpu_microcode is not _cpu['cpu_microcode']:
                db_device.cpu_microcode = _cpu['cpu_microcode']
            if db_device.cpu_arch is not _cpu['Architecture']:
                db_device.cpu_arch = _cpu['Architecture']
            if db_device.cpu_vendor_id is not _cpu['Vendor ID']:
                db_device.cpu_vendor_id = _cpu['Vendor ID']
            if db_device.cpu_brand is not _cpu['Model name']:
                db_device.cpu_brand = _cpu['Model name']
            if db_device.cpu_hz_advertised is not _cpu['CPU max MHz']:
                db_device.cpu_hz_advertised = _cpu['CPU max MHz']
            if db_device.cpu_hz_actual is not _cpu['BogoMIPS']:
                db_device.cpu_hz_actual = _cpu['BogoMIPS']
            if db_device.cpu_stepping is not _cpu['Stepping']:
                db_device.cpu_stepping = _cpu['Stepping']
            if db_device.cpu_model is not _cpu['Model']:
                db_device.cpu_model = _cpu['Model']
            if db_device.cpu_family is not _cpu['CPU family']:
                db_device.cpu_family = _cpu['CPU family']
            if db_device.cpu_physical_core_count is not _cpu['Core(s) per socket']:
                db_device.cpu_physical_core_count = _cpu['Core(s) per socket']
            if db_device.cpu_logical_core_count is not _cpu['CPU(s)']:
                db_device.cpu_logical_core_count = _cpu['CPU(s)']

            if db_device.installed_memory_total is not _installed_memory['total']:
                db_device.installed_memory_total = _installed_memory['total']
            if db_device.installed_memory_available is not _installed_memory['available']:
                db_device.installed_memory_available = _installed_memory['available']
            if db_device.installed_memory_used is not _installed_memory['used']:
                db_device.installed_memory_used = _installed_memory['used']
            if db_device.installed_memory_free is not _installed_memory['free']:
                db_device.installed_memory_free = _installed_memory['free']
            if db_device.installed_memory_percent is not _installed_memory['percent']:
                db_device.installed_memory_percent = _installed_memory['percent']

            if db_device.swap_memory_total is not _swap_memory['total']:
                db_device.swap_memory_total = _swap_memory['total']
            if db_device.swap_memory_used is not _swap_memory['used']:
                db_device.swap_memory_used = _swap_memory['used']
            if db_device.swap_memory_free is not _swap_memory['free']:
                db_device.swap_memory_free = _swap_memory['free']
            if db_device.swap_memory_percent is not _swap_memory['percent']:
                db_device.swap_memory_percent = _swap_memory['percent']

            if db_device.gpu is not _gpu_str:
                db_device.gpu = _gpu_str

            if db_device.disk_count is not len(_disk_info):
                db_device.disk_count = len(_disk_info)
            if db_device.disk_usage_total is not _disk_usage['total']:
                db_device.disk_usage_total = _disk_usage['total']
            if db_device.disk_usage_used is not _disk_usage['used']:
                db_device.disk_usage_used = _disk_usage['used']
            if db_device.disk_usage_free is not _disk_usage['free']:
                db_device.disk_usage_free = _disk_usage['free']
            if db_device.disk_usage_percent is not _disk_usage['percent']:
                db_device.disk_usage_percent = _disk_usage['percent']

            if db_device.uptime is not str(data["uptime"]):
                db_device.uptime = str(data["uptime"])
            db.session.add(db_device)

        ## Manage Disks
        db_disks = database.Disks.query.filter_by(host_id=host_id).all()
        db_partitions = database.DiskPartition.query.filter_by(host_id=host_id).all()
        _db_serial = set()
        _post_disk_serial = set()
        _post_partition = list()

        # check if incoming data are the same as in database
        for _, _item in _disk_info.items():
            _post_disk_serial.add(_item.get('disk_serial'))
            for _db_disk in db_disks:
                # Update Selected database if incoming data changed
                _db_serial.add(_db_disk.disk_serial)
                if _db_disk.disk_serial == _item.get('disk_serial'):
                    if _db_disk.disk_ptuuid != _item.get('disk_ptuuid'):
                        current_app.logger.debug(f"Drive PTUUID Changed: [ {_db_disk.disk_ptuuid} ]>[ "
                                                 f"{_item.get('disk_ptuuid')} ]  ")
                        _db_disk.disk_ptuuid = _item.get('disk_ptuuid')
                    elif _db_disk.disk_capacity != _item.get('disk_capacity'):
                        current_app.logger.debug(f"Drive PTUUID Changed: [ {_db_disk.disk_capacity} ]>[ "
                                                 f"{_item.get('disk_capacity')} ]  ")
                        _db_disk.disk_capacity = _item.get('disk_capacity')
                    elif _db_disk.disk_name != _item.get('disk_name'):
                        current_app.logger.debug(f"Drive PTUUID Changed: [ {_db_disk.disk_capacity} ]>[ "
                                                 f"{_item.get('disk_capacity')} ]  ")
                        _db_disk.disk_name = _item.get('disk_name')
                    elif _db_disk.disk_model != _item.get('disk_model'):
                        current_app.logger.debug(f"Drive PTUUID Changed: [ {_db_disk.disk_model} ]>[ "
                                                 f"{_item.get('disk_model')} ]  ")
                        _db_disk.disk_model = _item.get('disk_model')
                    elif _db_disk.disk_revision != _item.get('disk_revision'):
                        current_app.logger.debug(f"Drive PTUUID Changed: [ {_db_disk.disk_revision} ]>[ "
                                                 f"{_item.get('disk_revision')} ]  ")
                        _db_disk.disk_revision = _item.get('disk_revision')
                    elif _db_disk.disk_part_path != _item.get('disk_part_path'):
                        current_app.logger.debug(f"Drive PTUUID Changed: [ {_db_disk.disk_part_path} ]>[ "
                                                 f"{_item.get('disk_part_path')} ]  ")
                        _db_disk.disk_part_path = _item.get('disk_part_path')
                    elif _db_disk.disk_part_type != _item.get('disk_part_type'):
                        current_app.logger.debug(f"Drive PTUUID Changed: [ {_db_disk.disk_part_type} ]>[ "
                                                 f"{_item.get('disk_part_type')} ]  ")
                        _db_disk.disk_part_type = _item.get('disk_part_type')
                    elif _db_disk.disk_type != _item.get('disk_type'):
                        current_app.logger.debug(f"Drive PTUUID Changed: [ {_db_disk.disk_type} ]>[ "
                                                 f"{_item.get('disk_type')} ]  ")
                        _db_disk.disk_type = _item.get('disk_type')
                    elif _db_disk.disk_state != _item.get('disk_state'):
                        current_app.logger.debug(f"Drive PTUUID Changed: [ {_db_disk.disk_state} ]>[ "
                                                 f"{_item.get('disk_state')} ]  ")
                        _db_disk.disk_state = _item.get('disk_state')
                    db.session.add(_db_disk)

        # Add new drive
        if len(_disk_info) != len(db_disks):
            for drive in _post_disk_serial:
                # print(f"DRIVE: {drive}")
                if drive not in _db_serial:
                    
                    current_app.logger.debug(f"Drive detected with SN: [ {_disk_info[drive].get('disk_serial')}"
                                             f" ], Adding.")
                    disk_add = database.Disks(host_id = host_id,
                                    disk_ptuuid = _disk_info[drive].get('disk_ptuuid'),
                                    disk_capacity = _disk_info[drive].get('disk_capacity'),
                                    disk_name = _disk_info[drive].get('disk_name'),
                                    disk_model = _disk_info[drive].get('disk_model'),
                                    disk_revision = _disk_info[drive].get('disk_revision'),
                                    disk_serial = _disk_info[drive].get('disk_serial'),
                                    disk_part_path = _disk_info[drive].get('disk_part_path'),
                                    disk_part_type = _disk_info[drive].get('disk_part_type'),
                                    disk_type = _disk_info[drive].get('disk_type'),
                                    disk_state = _disk_info[drive].get('disk_state'),
                    )
                    db.session.add(disk_add)

                    # add partitions here
                    for _part in _disk_partition:
                        if _disk_info[drive].get('disk_ptuuid') == _disk_partition[_part].get('disk_id'):
                            _disk = list()
                            _disk.append(_disk_partition[_part].get('disk_id'))
                            _disk.append(_disk_partition[_part].get('disk_part_uuid'))
                            _post_partition.append(_disk)
                            current_app.logger.debug(f"Partition detected with ID: [ {_disk_partition[_part].get('disk_id')}"
                                             f" ], Adding.")
                            _add_part = database.DiskPartition(host_id = host_id,
                                            disk_id = _disk_partition[_part].get('disk_id'),
                                            disk_part_uuid = _disk_partition[_part].get('disk_part_uuid'),
                                            disk_label = _disk_partition[_part].get('disk_label'),
                                            disk_size_available = _disk_partition[_part].get('disk_size_available'),
                                            disk_size_used = _disk_partition[_part].get('disk_size_used'),
                                            disk_size_total = _disk_partition[_part].get('disk_size_total'),
                                            disk_revision = _disk_partition[_part].get('disk_revision'),
                                            disk_part_label = _disk_partition[_part].get('disk_part_label'),
                                            disk_part_number = _disk_partition[_part].get('disk_part_number'),
                                            disk_part_mounts = ' '.join(str(_disk_partition[_part].get('disk_part_mounts')).strip("[]")),
                                            disk_part_type = _disk_partition[_part].get('disk_part_type'),
                                            disk_filesystem_path = _disk_partition[_part].get('disk_filesystem_path'),
                                            )
                            db.session.add(_add_part)

        # Remove stale data from database
        stale_drives = _db_serial.difference(_post_disk_serial)
        for disk_item in db_disks:
            for stale_mount in stale_drives:
                if stale_mount == disk_item.disk_serial:
                    current_app.logger.debug(f"Drive with SN: [ {disk_item.disk_serial}"
                                             f" ] no longer available, Deleting.")
                    db.session.delete(disk_item)
                    
                    for part_item in db_partitions:
                        if disk_item.disk_ptuuid == part_item.disk_id:
                            current_app.logger.debug(f"Partition with ID: [ {disk_item.disk_serial}"
                                                     f" ] is no longer available, Deleting.")
                            db.session.delete(part_item)

        ## Manage Networks
        db_net = database.Networks.query.filter_by(network_id=host_id).all()
        
        # Get all new networks
        nets_in_db = set()
        all_current_nets = set()
        for net in _network:
            count = 0
            for item in db_net:
                if item.network_name == net:
                    try:
                        if int(item.network_number) != int(_network[net].get('network_number')):
                            current_app.logger.debug(f"[ {net} ] Network Number has changed [ "
                                    f"{item.network_number} ]>[ {_network[net].get('network_number')}"
                                    " ], Updating.")
                            item.network_number = _network[net].get('network_number')
                        elif (item.network_state).upper() != (_network[net].get('network_state')).upper():
                            current_app.logger.debug(f"[{net}] Network State has changed [ "
                                    f"{item.network_state} ]>[ {_network[net].get('network_state')}"
                                    " ], Updating.")
                            item.network_state = _network[net].get('network_state')
                        elif item.network_ipv4 != _network[net].get('network_ipv4'):
                            current_app.logger.debug(f"[{net}] Network IPv4 has changed [ "
                                    f"{item.network_ipv4} ]>[ {_network[net].get('network_ipv4')}"
                                    " ], Updating.")
                            item.network_ipv4 = _network[net].get('network_ipv4')
                        elif str(item.network_ipv6).lower() != str([v for v in _network[net].get('network_ipv6').values()]).strip("[]").lower():
                            current_app.logger.debug(f"[{net}] Network IPv6 has changed [ "
                                    f"{item.network_ipv6} ]>[ {str([v for v in _network[net].get('network_ipv6').values()]).strip("[]")}"
                                    " ], Updating.")
                            item.network_ipv6 = str([v for v in _network[net].get('network_ipv6').values()]).strip("[]")
                        elif item.network_mac != _network[net].get('network_mac'):
                            current_app.logger.debug(f"[{net}] Network {item.network_mac} MAC Address has changed, Updating.")
                            item.network_mac = _network[net].get('network_mac')
                    except KeyError as err:
                        current_app.logger.info(f"Error occurred during database update. Error {err}")

                db.session.add(item)
                nets_in_db.add(item.network_name)
                
            # check if new network the database and insert it if not
            if net not in nets_in_db:
                current_app.logger.debug(f"Adding new [ {net} ]  network.")
                new_net = database.Networks(
                    network_id = host_id,
                    network_number = _network[net].get('network_number'),
                    network_name = _network[net].get('network_name'),
                    network_state = _network[net].get('network_state'),
                    network_ipv4 = _network[net].get('network_ipv4'),
                    network_ipv6 = str([v for v in _network[net].get('network_ipv6').values()]).strip("[']"),
                    network_mac = _network[net].get('network_mac'),
                )
                db.session.add(new_net)
            all_current_nets.add(net)
            
        # Remove stale data from database
        stale_nets = nets_in_db.difference(all_current_nets)
        for item in db_net:
            for stale_name in stale_nets:
                if stale_name == item.network_name:
                    current_app.logger.info(f"Dropping [ {stale_name} ]")
                    db.session.delete(item)

        ## Manage Peripherals
        db_peripheral = database.PeripheralDevices.query.filter_by(peripheral_id=host_id).all()
        current_peripheral_number = 0
        new_periferals = set()

        # get all new peripheral devices
        for device in peripheral_devices:
            count = 0
            peripherals_in_db = set()

            for item in db_peripheral:
                if item.peripheral_device_id == device['id']:
                    if item.peripheral_number is not current_peripheral_number:
                        item.peripheral_number = current_peripheral_number
                    if item.peripheral_tag is not device['tag']:
                        item.peripheral_tag = device['tag']
                    if item.peripheral_device is not device['device']:
                        item.peripheral_device = device['device']
                    db.session.add(item)

                    if count != 0:
                        current_app.logger.debug(f"Deleting peripheral number"
                                                 f": {item.peripheral_number}"
                                                 f"- {item.peripheral_tag}")
                        db.session.delete(item)
                    count += 1
                peripherals_in_db.add(item.peripheral_device_id)

            if device['id'] not in peripherals_in_db:
                _peripheral_info = database.PeripheralDevices(peripheral_id = host_id,
                                    peripheral_number = current_peripheral_number,
                                    peripheral_device_id = device['id'],
                                    peripheral_tag = device['tag'],
                                    peripheral_device = device['device'])
                db.session.add(_peripheral_info)
            current_peripheral_number += 1
            new_periferals.add(device['id'])

        # Remove stale data from database
        stale_peripherals = peripherals_in_db.difference(new_periferals)
        for item in db_peripheral:
            for stale_device in stale_peripherals:
                if stale_device == item.peripheral_device_id:
                    db.session.delete(item)

        ## Manage Users
        db_users = database.DeviceUsers.query.filter_by(user_id=host_id).all()
        users_in_db = set()
        new_users = set()
        for _a_user in users:    
            for item in db_users:
                if int(_a_user) == int(item.user_number):
                    if item.user_name != users[_a_user].get('user_name'):
                        current_app.logger.debug(f"Username changed: {users[_a_user].get('user_name')}, Updating!")
                        item.user_name = users[_a_user].get('user_name')
                    if item.user_terminal != users[_a_user].get('user_terminal'):
                        current_app.logger.debug(f"Terminal has changed: {users[_a_user].get('user_terminal')}, Updating!")
                        item.user_terminal = users[_a_user].get('user_terminal')
                    if item.user_host_ip != users[_a_user].get('user_host_ip'):
                        current_app.logger.debug(f"User access terminal IP changed: {users[_a_user].get('user_host_ip')}, Updating!")
                        item.user_host_ip = users[_a_user].get('user_host_ip')
                    if item.user_time_login != users[_a_user].get('user_time_login'):
                        current_app.logger.debug(f"User Login Time: {users[_a_user].get('user_time_login')}, Updating!")
                        item.user_time_login = users[_a_user].get('user_time_login')
                    if item.user_time_idle != users[_a_user].get('user_time_idle'):
                        current_app.logger.debug(f"User Idle Time: {users[_a_user].get('user_time_idle')}, Updating!")
                        item.user_time_idle = users[_a_user].get('user_time_idle')
                    if item.user_what != users[_a_user].get('user_what'):
                        current_app.logger.debug(f"Access binary changed: {users[_a_user].get('user_what')}, Updating!")
                        item.user_what = users[_a_user].get('user_what')
                    if item.user_method != users[_a_user].get('user_method'):
                        current_app.logger.debug(f"Access method changed: {users[_a_user].get('user_method')}, Updating!")
                        item.user_method = users[_a_user].get('user_method')
                    db.session.add(item)
                users_in_db.add(int(item.user_number))

            if int(_a_user) not in users_in_db:
                current_app.logger.debug(f"New terminal user: '{users[_a_user].get('user_name')}'"
                                            f" detected, Inserting")
                add_user = database.DeviceUsers(user_id = host_id,
                                        user_number = _a_user,
                                        user_name = users[_a_user].get('user_name'),
                                        user_terminal = users[_a_user].get('user_terminal'),
                                        user_host_ip = users[_a_user].get('user_host_ip'),
                                        user_time_login = users[_a_user].get('user_time_login'),
                                        user_time_idle = users[_a_user].get('user_time_idle'),
                                        user_what = users[_a_user].get('user_what'),
                                        user_method = users[_a_user].get('user_method'),
                                        )
                db.session.add(add_user)
            new_users.add(int(_a_user))

            # Remove stale data from database
            stale_users = users_in_db.difference(new_users)
            for item in db_users:
                for stale in stale_users:
                    if int(stale) == int(item.user_number):
                        current_app.logger.debug(f"The '{item.user_name}'"
                                                f" is no longer logged in, Removing")
                        db.session.delete(item)

        db.session.commit()
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

    if request.method == 'DELETE':
        return json.dumps({'success':True}), 403, {'ContentType':'application/json'}
