function updatePlatform(elm) {
    var id = $(elm).attr('platform');
    $.ajax({
        type: 'POST',
        url: '/platform',
        contentType: 'application/json',
        data: {
            id: $(elm).attr('id'),
            prot: $(elm).text()
        },
        success: function (result) {
            console.log("Protocol-Data:", result);
            if (result.status == 'OK') {
                console.log("Status:", result.status);

                console.log("result:", result);
            } else {
                console.log('-- STATUS --', result.status);
                // $('#id').on('shown.bs.modal', function () {
                    // $('#myInput').trigger('focus')
                    // $('#id').modal('hide')
                // })
                // $('#delete-{{ current_link.id }}').modal('hide');
            }
        },
        error: function (error) {
            console.log(error);
        }
    });
};

var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "contents") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "contents";
    }
  });
} 