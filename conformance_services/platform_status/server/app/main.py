#!/usr/bin/env python3

# Copyright (C) Intel Corp.  2014.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Yuriy Bogdanov <yuriy.bogdanov@intel.com>

import os

from core import create_app
from logging.config import dictConfig

# Directive   Meaning Notes

# %a  Locale’s abbreviated weekday name.   
# %A  Locale’s full weekday name.  
# %b  Locale’s abbreviated month name.     
# %B  Locale’s full month name.    
# %c  Locale’s appropriate date and time representation.   
# %d  Day of the month as a decimal number [01,31].    
# %H  Hour (24-hour clock) as a decimal number [00,23].    
# %I  Hour (12-hour clock) as a decimal number [01,12].    
# %j  Day of the year as a decimal number [001,366].   
# %m  Month as a decimal number [01,12].   
# %M  Minute as a decimal number [00,59].  
# %p  Locale’s equivalent of either AM or PM. (1)
# %S  Second as a decimal number [00,61]. (2)
# %U  Week number of the year (Sunday as the first day of the week) as a decimal number [00,53]. All days in a new year preceding the first Sunday are considered to be in week 0.    (3)
# %w  Weekday as a decimal number [0(Sunday),6].   
# %W  Week number of the year (Monday as the first day of the week) as a decimal number [00,53]. All days in a new year preceding the first Monday are considered to be in week 0.    (3)
# %x  Locale’s appropriate date representation.    
# %X  Locale’s appropriate time representation.    
# %y  Year without century as a decimal number [00,99].    
# %Y  Year with century as a decimal number.   
# %z  Time zone offset indicating a positive or negative time difference from UTC/GMT of the form +HHMM or -HHMM, where H represents decimal hour digits and M represents decimal minute digits [-23:59, +23:59].  
# %Z  Time zone name (no characters if no time zone exists).   
# %%  A literal '%' character. 

dictConfig({
    'version': 1,
    'formatters': {'default': {
        # 'format': '%(asctime)s - %(levelname)s in %(module)s: %(message)s',
        'format': '%(asctime)s - %(levelname)s in PlatformStatus: %(message)s',
        'datefmt' : "%x %H:%M"
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})


app = create_app()

if __name__ == "__main__":
    if os.environ.get('APP_SETTINGS') == 'config.ProductionConfig':
        from waitress import serve
        print("Started Production Server")
        serve(app,host='0.0.0.0', port=os.environ.get("FLASK_SERVER_PORT", 10201))
    else:
        app.run(host='0.0.0.0', port=os.environ.get("FLASK_SERVER_PORT", 10201))
