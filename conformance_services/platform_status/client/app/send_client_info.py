#!/usr/bin/python3

import os
import platform
import time
import datetime
import re
import hashlib
import shutil
import sys
import subprocess
import json
import requests

platform_poll_time_sec = 10
update_cycle = 0 # 0 - is disabled

debug = False
localurl = 'http://127.0.0.1:10201'
serverurl = 'http://192.168.1.1:10201'
file_url = 'http://mesa-ci-files.jf.intel.com/client-info/app/send_client_info.py'
headers = {'Content-type': 'application/json', 'Accept-Encoding': 'gzip, deflate', 'Accept': 'text/plain'}

if debug:
    serverurl = localurl


def send_to_dmesg(message):
    process = subprocess.Popen(["dmesg", "-n"], stdin=subprocess.PIPE)
    process.communicate(input=message.encode())


def get_status():
    global platform_poll_time_sec
    try:
        global_conf = json.loads(requests.get(os.path.join(serverurl, 'version'),
                                              timeout=15).content.decode())
    except requests.exceptions.Timeout as err:
        print(f"ERROR: Failed to establish connection with error: {err}")

    if int(global_conf['time']) != platform_poll_time_sec and global_conf is not None:
        print(f"Adjusting time to {int(global_conf['time'])}")
        platform_poll_time_sec = int(global_conf['time'])

    local_file_sha = hashlib.sha256(open(os.path.abspath(__file__),
                                         'rb').read()).hexdigest()
    if not debug:
        if local_file_sha != global_conf['version']:
            print("New File detected, updating local file and restarting process")
            print(f"local sha:  {local_file_sha}")
            print(f"server sha: {global_conf['version']}")
            # attempt to auto update
            subprocess.check_output(f"wget {file_url} -O /usr/bin/send_client_info",
                                               universal_newlines=True,
                                               shell=True)
            sys.exit()


def list_sessions():
    output = subprocess.check_output(['w', '-h']).decode('utf-8')
    sessions = output.split('\n')
    for session in sessions:
        if session:
            return session


def get_system_info():
    info = dict()
    # Platform
    uname_result = platform.uname()
    _platform = dict()
    _platform['system'] = uname_result.system
    _platform['node'] = uname_result.node
    _platform['release'] = uname_result.release
    _platform['version'] = uname_result.version
    _platform['machine'] = uname_result.machine
    info['platform'] = _platform

    # Running processes/services do we need it?
    # _proc = list()
    # for p in psutil.process_iter():
    #     _proc.append(p.as_dict(attrs=['pid',
    #                                   'name',
    #                                   'username',
    #                                   'cpu_percent',
    #                                   'cpu_times',
    #                                   'memory_info'
    #                                   ]))
    # info['processes'] = _proc[0]

    # BIOS
    _bios = dict()
    _bios['vendor'] = subprocess.check_output("dmidecode --string bios-vendor",
                                              universal_newlines=True,
                                              shell=True).strip('\n')
    _bios['release_date'] = subprocess.check_output("dmidecode --string bios-release-date",
                                                    universal_newlines=True,
                                                    shell=True).strip('\n')
    _bios['version'] = subprocess.check_output("dmidecode --string bios-version",
                                               universal_newlines=True,
                                               shell=True).strip('\n')
    info['bios'] = _bios

    # CPU
    _cpu = dict()
    out = ((subprocess.check_output("lscpu", shell=True).strip()).decode()).split('\n')
    for item in out:
        new_item = (" ".join(item.split())).split(':')
        _cpu[new_item[0]] = new_item[-1]

    cpu_microcode = ((subprocess.check_output("grep -m 1 'microcode' /proc/cpuinfo",
                                              shell=True).strip()).decode()).split(':')
    _cpu['cpu_microcode'] = cpu_microcode[-1].strip()
    info['cpu'] = _cpu

    ## Memory
    mem = {}
    swap = {}
    freem = (subprocess.check_output("free -b", shell=True).strip()).decode().split('\n')
    freem.pop(0)
    for item in freem:
        out = (" ".join(item.split())).split(':')
        _mem_out = out[-1].strip().split(' ')
        if 'Mem' in out:
            _mem_out_percent = round((int(_mem_out[1]) / int(_mem_out[0])) * 100, 2)
            mem['total'] = _mem_out[0]
            mem['used'] = _mem_out[1]
            mem['free'] = _mem_out[2]
            mem['shared'] = _mem_out[3]
            mem['cached'] = _mem_out[4]
            mem['available'] = _mem_out[5]
            mem['percent'] = _mem_out_percent
    if 'Swap' in out:
        _mem_out_percent = round((int(_mem_out[1]) / int(_mem_out[0])) * 100, 2)
        swap['total'] = _mem_out[0]
        swap['used'] = _mem_out[1]
        swap['free'] = _mem_out[2]
        swap['percent'] = _mem_out_percent
    info['installed_memory'] = mem
    info['swap_memory'] = swap

    ## Disks
    _all_disks = dict()
    _partition = dict()
    disk_info = json.loads((subprocess.check_output("lsblk -JO", shell=True).strip()).decode())

    disk_count = 0
    total_partitions = 0
    for item in disk_info['blockdevices']:
        _disk = dict()
        if 'loop' not in item['name']:
            disk_count += 1
            _disk['disk_model'] = item['model']
            _disk['disk_revision'] = item['rev']
            _disk['disk_serial'] = item['serial']
            _disk['disk_capacity'] = item['size']
            _disk['disk_name'] = item['name']
            _disk['disk_part_path'] = item['path']
            _disk['disk_ptuuid'] = item['ptuuid']
            _disk['disk_part_type'] = item['pttype']
            _disk['disk_state'] = item['state']
            _disk['disk_type'] = item['tran']
            _all_disks[item['serial']] = _disk

            for child in item.get('children'):
                _child = dict()
                total_partitions += 1
                _child['disk_part_number'] = child.get('partn')
                _child['disk_id'] = child.get('ptuuid')
                _child['disk_part_uuid'] = child.get('uuid')
                _child['disk_label'] = child.get('label')
                _child['disk_part_label'] = child.get('partlabel')
                _child['disk_size_available'] = child.get('fsavail')
                _child['disk_size_used'] = child.get('fsused')
                _child['disk_part_type'] = child.get('fstype')
                _child['disk_revision'] = child.get('maj:min')
                _child['disk_filesystem_path'] = child.get('path')
                _child['disk_size_total'] = child.get('size')
                _child['disk_part_mounts'] = child.get('mountpoints')
                _partition[total_partitions] = _child

        info['disk_info'] = _all_disks
        info['disk_partitions'] = _partition
        
    # add disk usage
    disk_usage = {}
    total, used, free = shutil.disk_usage("/")
    percent = round((used / total) * 100, 2)
    disk_usage['total'] = total
    disk_usage['used'] = used
    disk_usage['free'] = free
    disk_usage['percent'] = percent
    info['disk_usage'] = disk_usage

    # GPU
    gpu = subprocess.check_output(
        r"lspci | grep ' VGA ' | cut -d' ' -f1 | xargs -i lspci -v -s {}",
        universal_newlines=True,
        shell=True
        )
    info['gpu'] = gpu.strip().split('\n')

    # Network
    _network = dict()
    out = json.loads((subprocess.check_output("ip -j a", shell=True).strip()).decode())
    for item in out:
        _interface = dict()
        _interface['network_number'] = item['ifindex']
        _interface['network_name'] = item['ifname']
        try:
            _interface['network_mac'] = item['address']
            
        except:
            pass
        _interface['network_state'] = item['operstate']
        try:
            pv6 = dict()
            for ip_count in range(len(item['addr_info'])):
                if len(item['addr_info'][ip_count-1]['local']) > 7 \
                and len(item['addr_info'][ip_count-1]['local']) <= 15:
                    _interface['network_ipv4'] = item['addr_info'][ip_count-1]['local']
                else:
                    pv6[ip_count] = item['addr_info'][ip_count-1]['local']
            _interface['network_ipv6'] = pv6
        except Exception as err:
            print(err)
            pass
        _network[item['ifname']] = _interface
    info['network'] = _network

    # Peripherals (USB connected devices)
    _devices = []
    device_re = re.compile(
        r"Bus\s+(?P<bus>\d+)\s+Device\s+(?P<device>\d+).+ID\s(?P<id>\w+:\w+)\s(?P<tag>.+)$",
        re.I
        )
    df = subprocess.check_output("lsusb", universal_newlines=True)
    for i in df.split('\n'):
        if i:
            _inf = device_re.match(i)
            if _inf:
                dinfo = _inf.groupdict()
                dinfo['device'] = f"/dev/bus/usb/{dinfo.pop('bus')}/{dinfo.pop('device')}"
                _devices.append(dinfo)
    info['peripheral_devices'] = _devices

    # Uptime
    time_since = datetime.datetime.strptime((subprocess.check_output("uptime -s",
                                                                 shell=True).strip()).decode(),
                                        '%Y-%m-%d %H:%M:%S')
    uptime = str(datetime.timedelta(seconds = int(time.time() - time_since.timestamp())))
    info['uptime'] = uptime

    # Users
    _users = dict()
    users = (subprocess.check_output("w -h", shell=True).strip()).decode()
    for num, user in enumerate(users.split('\n')):
        cur_usr = dict()
        if len(user.split()) == 9:
            cur_usr['user_name'], \
            cur_usr['user_terminal'], \
            cur_usr['user_host_ip'], \
            cur_usr['user_time_login'], \
            cur_usr['user_time_idle'], \
            _, _, \
            cur_usr['user_what'], \
            cur_usr['user_method'], = user.split()
            _users[num] = cur_usr
        elif len(user.split()) == 10:
            cur_usr['user_name'], \
            cur_usr['user_terminal'], \
            cur_usr['user_host_ip'], \
            cur_usr['user_time_login'], \
            cur_usr['user_time_idle'], \
            _, _, \
            cur_usr['user_what'], \
            command, flags = user.split()
            cur_usr['user_method'] = f"{command} {flags}"
            _users[num] = cur_usr
    info['users'] = _users
    return info

try:
    while True:
        if update_cycle != 0: # Dev loop
            for cycle in range(update_cycle):
                get_status()
                try:
                    r = requests.post(os.path.join(serverurl, 'platform'),
                                    data=json.dumps(get_system_info()),
                                    headers=headers,
                                    timeout=15
                                    )
                except requests.exceptions.ConnectionError as err:
                    print(f"Could not connect to server.\
                        Trying again in {int( platform_poll_time_sec / 60) } min.")
                print(f"sleeping for: {platform_poll_time_sec} sec, Loop #: {cycle}")
                time.sleep(platform_poll_time_sec)
            break
        else: # Prod loop
            get_status()
            try:
                r = requests.post(os.path.join(serverurl, 'platform'),
                                    data=json.dumps(get_system_info()),
                                    headers=headers,
                                    timeout=15
                                    )
            except requests.exceptions.ConnectionError as err:
                print(f"Could not connect to server.\
                    Trying again in {int( platform_poll_time_sec / 60) } min.")

            # print(f"sleeping for: {platform_poll_time_sec} sec")
            time.sleep(platform_poll_time_sec)

except KeyboardInterrupt:
    print("Stopping Service, Send to server")

# ToDo proper logging and main loop
# def main():
#     try:
#         shell()
#     except KeyboardInterrupt:
#         printer('\nCancelling...', error=True)
#     except (SpeedtestException, SystemExit):
#         e = get_exception()
#         # Ignore a successful exit, or argparse exit
#         if getattr(e, 'code', 1) not in (0, 2):
#             msg = '%s' % e
#             if not msg:
#                 msg = '%r' % e
#             raise SystemExit('ERROR: %s' % msg)


# if __name__ == '__main__':
#     main()
