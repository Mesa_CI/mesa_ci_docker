Client <--------------------|
  |                         |
  V                         |
Server -> Web UI info       |
               |            |
               V            |
            Basic Settings -|
                |
                V
    1. Refresh interval
    2. Info
        2.1 CPU
        2.2 Kernel
        ...
        2.10 BIOS Ver.
        ... ETC.
    3. Update Kernel from location???
    4. Update BIOS from location if available



### Client
---
#### Gather system information
Platform info
 - CPU
 - RAM
 - BIOS ver.
 - Storage capacity
 - Kernel ver.


### Server
---
#### Server Configuration options

```
docker exec status_web flask db init
docker exec status_web flask db migrate
docker exec status_web flask db upgrade
```
