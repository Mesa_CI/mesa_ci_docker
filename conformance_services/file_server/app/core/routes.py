import humanize
import json
import mimetypes
import os
import re
import stat
from core import app
from datetime import datetime
from flask import Flask, make_response, request, session, render_template, Response, send_from_directory
from flask.views import MethodView
from pathlib import Path
from werkzeug.utils import secure_filename


root = os.path.normpath("/files")
key = ""

ignored = ['.bzr', '$RECYCLE.BIN', '.DAV', '.DS_Store', '.git', '.hg', '.htaccess', '.htpasswd', '.Spotlight-V100', '.svn', '__MACOSX', 'ehthumbs.db', 'robots.txt', 'Thumbs.db', 'thumbs.tps']
datatypes = {'audio': 'm4a,mp3,oga,ogg,webma,wav', 'archive': '7z,zip,rar,gz,tar', 'image': 'gif,ico,jpe,jpeg,jpg,png,svg,webp', 'pdf': 'pdf', 'quicktime': '3g2,3gp,3gp2,3gpp,mov,qt', 'source': 'atom,bat,bash,c,cmd,coffee,css,hml,js,json,java,less,markdown,md,php,pl,py,rb,rss,sass,scpt,swift,scss,sh,xml,yml,plist', 'text': 'txt', 'video': 'mp4,m4v,ogv,webm', 'website': 'htm,html,mhtm,mhtml,xhtm,xhtml'}
icontypes = {'fa-music': 'm4a,mp3,oga,ogg,webma,wav', 'fa-archive': '7z,zip,rar,gz,tar,xz,deb', 'fa-picture-o': 'gif,ico,jpe,jpeg,jpg,png,svg,webp', 'fa-file-text': 'pdf', 'fa-film': '3g2,3gp,3gp2,3gpp,mov,qt', 'fa-code': 'atom,plist,bat,bash,c,cmd,coffee,css,hml,js,json,java,less,markdown,md,php,pl,py,rb,rss,sass,scpt,swift,scss,sh,xml,yml', 'fa-file-text-o': 'txt', 'fa-wheelchair' : 'trace,trace6', 'fa-film': 'mp4,m4v,ogv,webm', 'fa-globe': 'htm,html,mhtm,mhtml,xhtm,xhtml'}


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
           'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.template_filter('size_format')
def size_format(size):
    return humanize.naturalsize(size)

@app.template_filter('time_format')
def time_desc(timestamp):
    mdate = datetime.fromtimestamp(timestamp)
    str = mdate.strftime('%Y-%m-%d %H:%M:%S')
    return str

@app.template_filter('data_format')
def data_format(filename):
    file_type = 'unknown'
    for type, exts in datatypes.items():
        if filename.split('.')[-1] in exts:
            file_type = type
    return file_type

@app.template_filter('icon_format')
def icon_format(filename):
    file_icon = 'fa-file-o'
    for icon, exts in icontypes.items():
        if filename.split('.')[-1] in exts:
            file_icon = icon
    return file_icon

@app.template_filter('humanize')
def time_humanize(timestamp):
    mdate = datetime.utcfromtimestamp(timestamp)
    return humanize.naturaltime(mdate)

def get_type(file_type):
    if stat.S_ISDIR(file_type) or stat.S_ISLNK(file_type):
        type = 'dir'
    else:
        type = 'file'
    return type

def partial_response(path, start, end=None):
    file_size = os.path.getsize(path)

    if end is None:
        end = file_size - start - 1
    end = min(end, file_size - 1)
    length = end - start + 1

    with open(path, 'rb') as file_read:
        file_read.seek(start)
        bytes = file_read.read(length)
    assert len(bytes) == length

    response = Response(
        bytes,
        206,
        mimetype=mimetypes.guess_type(path)[0],
        direct_passthrough=True,
    )
    response.headers.add(
        'Content-Range', 'bytes {0}-{1}/{2}'.format(
            start, end, file_size,
        ),
    )
    response.headers.add(
        'Accept-Ranges', 'bytes'
    )
    return response

def get_range(request):
    range = request.headers.get('Range')
    m = re.match('bytes=(?P<start>\d+)-(?P<end>\d+)?', range)
    if m:
        start = m.group('start')
        end = m.group('end')
        start = int(start)
        if end is not None:
            end = int(end)
        return start, end
    else:
        return 0, None

class PathView(MethodView):
    def get(self, p=''):
        hide_dotfile = request.args.get('hide-dotfile', request.cookies.get('hide-dotfile', 'no'))

        path = os.path.join(root, p)

        if os.path.isdir(path):
            contents = []
            total = {'size': 0, 'dir': 0, 'file': 0}
            for file_name in os.listdir(path):
                if file_name in ignored:
                    continue
                if hide_dotfile == 'yes' and file_name[0] == '.':
                    continue
                filepath = os.path.join(path, file_name)
                stat_res = os.stat(filepath)
                info = {}
                info['name'] = file_name
                info['mtime'] = stat_res.st_mtime
                ft = get_type(stat_res.st_mode)
                info['type'] = ft
                total[ft] += 1
                sz = stat_res.st_size
                info['size'] = sz
                total['size'] += sz
                contents.append(info)
            page = render_template('index.html', path=p, contents=contents, total=total, hide_dotfile=hide_dotfile)
            response = make_response(page, 200)
            response.set_cookie('hide-dotfile', hide_dotfile, max_age=16070400)
        elif os.path.isfile(path):
            if 'Range' in request.headers:
                start, end = get_range(request)
                response = partial_response(path, start, end)
            else:
                print(f"SENDING FILE PATH {path} {root} {p}")
                response = send_from_directory(root, p)
        else:
            response = make_response('Not found', 404)
        return response
    
    # def put(self, p=''):
    #     path = os.path.join(root, p)
    #     dir_path = os.path.dirname(path)
    #     Path(dir_path).mkdir(parents=True, exist_ok=True)

    #     info = {}
    #     if os.path.isdir(dir_path):
    #         try:
    #             file_name = secure_filename(os.path.basename(path))
    #             with open(os.path.join(dir_path, file_name), 'wb') as f:
    #                 f.write(request.stream.read())
    #         except Exception as e:
    #             info['status'] = 'error'
    #             info['msg'] = str(e)
    #         else:
    #             info['status'] = 'success'
    #             info['msg'] = 'File Saved'
    #     else:
    #         info['status'] = 'error'
    #         info['msg'] = 'Invalid Operation'
    #     response = make_response(json.JSONEncoder().encode(info), 201)
    #     response.headers.add('Content-type', 'application/json')
    #     return response

    # def post(self, p=''):
    #     path = os.path.join(root, p)
    #     Path(path).mkdir(parents=True, exist_ok=True)

    #     info = {}
    #     if os.path.isdir(path):
    #         files = request.files.getlist('files[]')
    #         for file in files:
    #             try:
    #                 file_name = secure_filename(file.file_name)
    #                 file.save(os.path.join(path, file_name))
    #             except Exception as e:
    #                 info['status'] = 'error'
    #                 info['msg'] = str(e)
    #             else:
    #                 info['status'] = 'success'
    #                 info['msg'] = 'File Saved'
    #     else:
    #         info['status'] = 'error'
    #         info['msg'] = 'Invalid Operation'
    #     response = make_response(json.JSONEncoder().encode(info), 200)
    #     response.headers.add('Content-type', 'application/json')
    #     return response
    
    # def delete(self, p=''):
    #     path = os.path.join(root, p)
    #     dir_path = os.path.dirname(path)
    #     Path(dir_path).mkdir(parents=True, exist_ok=True)

    #     info = {}
    #     if os.path.isdir(dir_path):
    #         try:
    #             file_name = secure_filename(os.path.basename(path))
    #             os.remove(os.path.join(dir_path, file_name))
    #             os.rmdir(dir_path)
    #         except Exception as e:
    #             info['status'] = 'error'
    #             info['msg'] = str(e)
    #         else:
    #             info['status'] = 'success'
    #             info['msg'] = 'File Deleted'
    #     else:
    #         info['status'] = 'error'
    #         info['msg'] = 'Invalid Operation'
    #     response = make_response(json.JSONEncoder().encode(info), 204)
    #     response.headers.add('Content-type', 'application/json')
    #     return response
