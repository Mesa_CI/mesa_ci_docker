#!/bin/bash
set -e

python3 get_token.py
docker-compose "$@"
