mesa ci services configuration:
- clone this repo into /var/cache/mesa_ci_docker
- set jenkins_auth_scp_path in vars.yml
- start the services: ./docker-compose.sh up -d