A collection of Dockerfiles used for generating container images used in the
Mesa CI

perf-steam_no_runtime - required infrastructure to allow perf testers to run performance test suites
conformance_services - all services needed to run an instance of intel mesa CI's jenkins setup (jenkins, rsync, nginx, etc)
